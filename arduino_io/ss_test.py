# This module will help debug protocol issues
import arduino_io as com
import time

#API that we need to check
# enum, read, write, close
devs = com.enum()
if len(devs) == 0:
    print("Failed to detect any devices")
    exit()

while 1:
    data = []
    for i in range(len(devs)):
        com.read(devs[i], 1, 1, data)
        reg0 = data[0]
        print(hex(reg0))
    print(".................")